// Copyright Oleg Samokhin 2019

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TaskComponent.generated.h"

/**
 * 
 */
UCLASS()
class BUILDINGESCAPE_API UTaskComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	
};
