// Copyright Oleg Samokhin 2019

#pragma once
#include "Engine.h"
//#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	float Reach = 100.f;

	UPhysicsHandleComponent * PhysicsHandle = nullptr;
	UInputComponent * InputComp = nullptr;
	void Grab(); // Ray-cast and gtab what's in reach
	void Release(); // Called when Grab is released
	void FindPhysicsHandleComponent(); // Find	attached physics handle
	void SetupInputComponent();  // Setup (assumed) attached input component
	FHitResult GetFirstPhysicsBodyInReach() const; // Return hit for first physics body in reach
};
